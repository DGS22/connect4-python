import os
import rules_connect4_english
from colorama import Fore,init
init(autoreset=True) 


def crearTablero(filas,columnas):
    tablero = []
    num = 0
    for x in range(filas+1):
        tablero.append([])
        num = 0
        for n in range(columnas+2):
            if n > 0:
                num = num + 1
            if x == filas and n != 0 and n != columnas+1:
                tablero[x].append(num)
            elif n == 0: 
                tablero[x].append("|")
            elif n == columnas+1:
                tablero[x].append("|")
            else: 
                tablero[x].append(".")
    return tablero


def mostrarTablero(tablero):
    for x in tablero: 
        print()
        for n in x:
            print('  ',n, end = "")
        print()


def validarFila(filas,columnas,tablero,columna):
    validar = True
    borde = False
    for x in range(filas+1):
        for y in range(columnas+2):
            if columna != 0 and columna <= columnas:
                if tablero[0][columna] != '.':
                    validar = False
                elif tablero[0][columna] == '.':
                    validar = True
                elif tablero[0][columna] == '|':
                    borde = True
            else: 
                validar = True
    if borde == True:
        return borde
    else: 
        return validar


def pedirCoord(filas,columnas,tablero):
    columna_valida = input(Fore.LIGHTBLUE_EX + f"Indica la columna donde vas ha dejar caer la ficha, elije entre (1 - {columnas}): ")
    while columna_valida.isnumeric() == False:
        print("\nLo que introduces debe ser un valor numérico\n")
        columna_valida = input(Fore.LIGHTBLUE_EX + f"Indica la columna donde vas ha dejar caer la ficha, elije entre (1 - {columnas}): ")
    columna_valida = int(columna_valida)
    validar = validarFila(filas,columnas,tablero,columna_valida)
    while validar != True or (columna_valida > columnas or columna_valida < 1):
        if validar != True:
            print(Fore.RED + "Esta columna está llena, elije otra\n")
            mostrarTablero(tablero)
            print("\n")
        elif columna_valida > columnas or columna_valida < 1:
            print(Fore.RED + f"\nEsta columna no es válida, elije otra entre (1 - {columnas})\n")
        columna_valida = int(input(Fore.LIGHTBLUE_EX + f"Indica la columna donde vas ha dejar caer la ficha, elije entre (1 - {columnas}): "))
        validar = validarFila(filas,columnas,tablero,columna_valida)
    print(Fore.GREEN + f"\nLa columna {columna_valida} está dentro del tablero, entonces:\n")
    return columna_valida


def fichas(jugador, ficha1, ficha2):
    if jugador == 0:
        ficha = Fore.RED + ficha1
    if jugador == 1:
        ficha = Fore.RED + ficha1
    elif jugador == 2:
        ficha = Fore.BLUE + ficha2
    return ficha


def colocarFicha(coord, filas, columnas, jugador, tablero, ficha1, ficha2):
    fichita = fichas(jugador, ficha1, ficha2)
    disponible = 0
    for x in range(filas):
        for y in range(columnas+1):
            if y > 0 and y < columnas+1 and x < filas:
                if y == coord:
                    if fichita == Fore.RED + ficha1:
                        if tablero[x][coord] == '.':
                            disponible = disponible
                        else:
                            disponible += 1
                    elif fichita == Fore.BLUE + ficha2:
                        if tablero[x][coord] == '.':
                            disponible = disponible
                        else:
                            disponible += 1
    tablero[(filas-1)-disponible][coord] = fichita


def victoria(tablero, fichita, jugador, ficha1, ficha2):
    fichita = fichas(jugador, ficha1, ficha2)
    altura = len(tablero)
    ancho = len(tablero[0])
    ganar = False
    tipovictoria = ""
    #comprobación_victoria_horizontal
    for x in range(altura):
        for y in range(ancho - 3):
            if tablero[x][y] == fichita and tablero[x][y+1] == fichita and tablero[x][y+2] == fichita and tablero[x][y+3] == fichita:
                ganar = True
                tipovictoria = Fore.GREEN + 'Enhorabuena, has ganado en dirección horizontal'

    #comprobación_victoria_vertical
    for x in range(altura - 3):
        for y in range(ancho):
            if tablero[x][y] == fichita and tablero[x+1][y] == fichita and tablero[x+2][y] == fichita and tablero[x+3][y] == fichita:
                ganar = True
                tipovictoria = Fore.GREEN + 'Enhorabuena, has ganado en dirección vertical'

    #comprobación_victoria_diagonal_derecha
    for x in range(3, altura):
        for y in range(ancho - 3):
            if tablero[x][y] == fichita and tablero[x-1][y+1] == fichita and tablero[x-2][y+2] == fichita and tablero [x-3][y+3] == fichita:
                ganar = True
                tipovictoria = Fore.GREEN + 'Enhorabuena, has ganado en dirección diagonal-derecha'

    #comprobación_victoria_diagonal_izquierda
    for x in range(altura - 3):
        for y in range(ancho - 3):
            if tablero[x][y] == fichita and tablero[x+1][y+1] == fichita and tablero[x+2][y+2] == fichita and tablero [x+3][y+3] == fichita:
                ganar = True
                tipovictoria = Fore.GREEN + 'Enhorabuena, has ganado en dirección diagonal-izquierda'

    return ganar,tipovictoria


def tableroCompleto(tablero):
    altura = len(tablero)
    ancho = len(tablero[0])
    cont = 0
    for x in range(altura):
        for y in range(ancho):
            if tablero[x][y] != '.':
                cont += 1
    if cont == (altura*ancho):
        return True
    elif cont != (altura*ancho): 
        return False


def turno(jugador, ficha1, ficha2):
    if jugador == 1:
        jugador = 2
        print(Fore.YELLOW + "\nFicha:",ficha2)
    elif jugador == 2 or jugador == 0: 
        jugador = 1
        print(Fore.YELLOW + "\nFicha:",ficha1)
    print(Fore.YELLOW + "Turno del jugador #:",jugador,"\n")
    return jugador


def jugar(jugador, filas, columnas, tablero, ficha1, ficha2):
    f1 = ficha1
    f2 = ficha2
    ficha = fichas(jugador,f1,f2)
    while (victoria(tablero,ficha,jugador,f1,f2)[0] == False and tableroCompleto(tablero) == False):
        jugador = turno(jugador,f1,f2)
        coord = pedirCoord(filas,columnas,tablero)
        colocarFicha(coord, filas,columnas,jugador,tablero,f1,f2)
        mostrarTablero(tablero)
    ficha = fichas(jugador,f1,f2)
    if tableroCompleto(tablero) == True:
        print(Fore.CYAN + "\nEl tablero se ha rellenado por completo, es un empate, nadie ganará el punto\n")
        print(Fore.CYAN + "Te apetece jugar de nuevo?")
        return 0
    elif victoria(tablero,ficha,jugador,f1,f2)[0] == True:
        print(Fore.LIGHTMAGENTA_EX + "\nVictoria del jugador:",jugador,Fore.LIGHTMAGENTA_EX + "con la ficha:",ficha)
        print(victoria(tablero,ficha,jugador,f1,f2)[1],"\n")
        print(Fore.CYAN + "Te apetece jugar de nuevo?")
        return jugador


def comprobarFichas():
    ficha1 = input("Ficha para el jugador #1: ")
    ficha2 = input("Ficha para el jugador #2: ")
    while (len(ficha1) > 1 or len(ficha2) > 1 or ficha1 == " " or ficha2 == " " or ficha1 == "" or ficha2 == "" or ficha1 == "." or ficha2 == "."):
        if len(ficha1) > 1 or len(ficha2) > 1:
            print(Fore.RED + "\nLa longitud de la ficha no puede superar un caráctero\n")
            ficha1 = input("Ficha para el jugador #1: ")
            ficha2 = input("Ficha para el jugador #2: ")
        elif ficha1 == " " or ficha2 == " ":
            print(Fore.RED + "\nLa ficha no puede ser un espacio en blanco\n")
            ficha1 = input("Ficha para el jugador #1: ")
            ficha2 = input("Ficha para el jugador #2: ")
        elif ficha1 == "" or ficha2 == "":
            print(Fore.RED + "\nLa ficha no es válida\n")
            ficha1 = input("Ficha para el jugador #1: ")
            ficha2 = input("Ficha para el jugador #2: ")
        elif ficha1 == "." or ficha2 == ".":
            print(Fore.RED + "\nLa ficha no puede ser un punto\n")
            ficha1 = input("Ficha para el jugador #1: ")
            ficha2 = input("Ficha para el jugador #2: ")
    return ficha1,ficha2


def comprobarFilasColumnas():
    print(Fore.LIGHTBLUE_EX + "\nSelecciona el número de filas y columnas para el tablero: \n")
    filas = int(input("         Filas: "))
    columnas = int(input("         Columnas: "))
    while filas < 4 or columnas < 4:
        if filas >= 4 and columnas < 4:
            print(Fore.RED + "\nHay más de 4 filas aunque las columnas no tiene\n")
            filas = int(input("         Filas: "))
            columnas = int(input("         Columnas: "))
        elif filas < 4 and columnas >= 4:
            print(Fore.RED + "\nHay más de 4 columnas aunque las filas no tiene\n")
            filas = int(input("         Filas: "))
            columnas = int(input("         Columnas: "))
        elif filas < 4 or columnas < 4:
            print(Fore.RED + "\nEl tablero mínimo tiene que ser 4x4 o más\n")
            filas = int(input("         Filas: "))
            columnas = int(input("         Columnas: "))
    print(Fore.GREEN + "\nBien..sigamos\n")
    return filas,columnas


def crearPartida():
    filas,columnas = comprobarFilasColumnas()
    ficha1,ficha2 = comprobarFichas()
    while ficha1 == ficha2:
        print(Fore.RED + "\nLas fichas no pueden ser las mismas para los 2 jugadores\n")
        ficha1,ficha2 = comprobarFichas()
    tablero = crearTablero(filas,columnas)
    print(Fore.GREEN + "\nBien..sigamos\n")
    return filas,columnas,ficha1,ficha2,tablero


def resultado(ganador,partidas,rangos):
    jugador1 = 0
    jugador2 = 0
    rangos[partidas] = ganador
    for key in rangos.keys():
        if rangos[key] == 1:
            jugador1 += 1
        elif rangos[key] == 2:
            jugador2 += 1
    return jugador1,jugador2


def menu():
    opcion = int(input(Fore.LIGHTWHITE_EX + "1. Crear Tablero\n2. Mostrar Tablero\n3. Jugar\n4. Resultados\n0. Exit\nElección: "))
    while opcion < 0 or opcion > 4:
        print(Fore.RED + "\nLa opción elegida tiene que ser entre 0 y 4\n")
        opcion = int(input(Fore.LIGHTWHITE_EX + "1. Crear Tablero\n2. Mostrar Tablero\n3. Jugar\n4. Resultados\n0. Exit\nElección: "))
    return opcion


def main():
    global tablero
    opcion = menu()
    creado = False
    jugado = False
    jugador = 0
    partidas = 1
    victorias1 = 0
    victorias2 = 0
    rangos = {}
    while opcion != 0:
        if opcion == 1:
            filas,columnas,ficha1,ficha2,tablero = crearPartida()
            creado = True
        elif opcion == 2:   
            if creado == False:
                print(Fore.YELLOW + "\nVuelve a jugar para mostrar el nuevo tablero")
                filas,columnas,ficha1,ficha2,tablero = crearPartida()
                creado = True
            elif creado == True:
                mostrarTablero(tablero)
                print("\n")
        elif opcion == 3:
            if creado == False:
                print(Fore.YELLOW + "\nPrimero tienes que crear el tablero")
                filas,columnas,ficha1,ficha2,tablero = crearPartida()
                creado = True
                print(Fore.YELLOW + "Este es vuestro tablero para jugar:")
                mostrarTablero(tablero)
                print("\n")
                ganador = jugar(jugador,filas,columnas,tablero,ficha1,ficha2)
                if ganador == 1 or ganador == 2:
                    victorias1,victorias2 = resultado(ganador,partidas,rangos)
                    partidas = partidas + 1
                    jugado = True
                elif ganador == 0:
                    print()
                creado = False
                print(Fore.YELLOW + "Vuelve a crear un tablero para jugar de nuevo\n")
            elif creado == True:
                ganador = jugar(jugador,filas,columnas,tablero,ficha1,ficha2)
                if ganador == 1 or ganador == 2:
                    victorias1,victorias2 = resultado(ganador,partidas,rangos)
                    partidas = partidas + 1
                    jugado = True
                elif ganador == 0:
                    print()
                creado = False
                print(Fore.YELLOW + "Vuelve a crear un tablero para jugar de nuevo\n")
        elif opcion == 4:
            if jugado == False:
                print(Fore.YELLOW + "\nPrimero tienes que jugar al menos una partida para ver los resultados")
                filas,columnas,ficha1,ficha2,tablero = crearPartida()
                creado = True
                print(Fore.YELLOW + "\nEste es vuestro tablero para jugar:")
                mostrarTablero(tablero)
                print("\n")
                ganador = jugar(jugador,filas,columnas,tablero,ficha1,ficha2)
                if ganador == 1 or ganador == 2:
                    victorias1,victorias2 = resultado(ganador,partidas,rangos)
                    partidas = partidas + 1
                    jugado = True
                elif ganador == 0:
                    print()
                creado = False
                print(Fore.YELLOW + "Vuelve a crear un tablero para jugar de nuevo\n")
            elif jugado == True:
                print(Fore.LIGHTMAGENTA_EX + "\nJugador #1 victorias: ",victorias1,Fore.LIGHTMAGENTA_EX + "    Jugador #2 victorias: ",victorias2,"\n")
        opcion = menu()
    print(Fore.RED + "Gracias por jugar!\n\n".center(140))


main()
