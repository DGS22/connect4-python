ENLACE VIDEO TRAILER: https://www.youtube.com/watch?v=wf-BPy8EnX4

!! Instalar la librería "colorama" para poder jugar, comando: "pip install colorama" !!

Bienvenid@
Esto es una pequeña introducción sobre el juego Conecta 4

Lo que vas a leer a continuación es casi lo mismo que las reglas que te van ha aparecer una vez inicias el juego.
    (Para hacer esto, se trata de una importación de otro documento al main principal.)

Reglas: 

    - 2 jugadores que tenéis el derecho de elegir vuestra ficha a vuestro gusto, no podeis utilizar las mismas fichas, ni espacios en blanco
    ni puntos.
    - el tamaño del tablero lo eliges tu, lo más recomendable sería entre 4x4 y 9x9, menor a 4x4 no puede ser.
    - tenéis que ir colocando fichas en distintas columnas para conseguir una línea de 4 fichas seguidas, independientemente de la direccíon
    eso quiere decir que puedes ganar tanto en horizontal, vertical como diagonal.
    - el primero que consigue ganar se lleva un punto, puedes hacer una competición con tu compañero para demonstrar quien es el mejor.
    - el código tiene implementado varios controles de errores, si te equívocas o bien peta el código, o bien se te puede proporcionar ayuda.

Funcionamiento:

    - intenta seguir el orden de ejecución para que todo sea fluydo y sin problemas.

Espero que disfrutes del código y gracias por tu anticipación.


